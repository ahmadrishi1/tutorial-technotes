const User = require('../models/User')
const Note = require('../models/Note')
const asyncHandler = require('express-async-handler')

// @desc GET all notes
// @route GET /notes
// @access Private

const getAllNotes = asyncHandler(async (req, res) => {
    const notes = await Note.find().select().lean()
    if(!notes?.length) {
        return res.status(400).json({message: "No notes Found!"})
    }

    res.json(notes)
})

// @desc POST create new note
// @route POST /notes
// @access Private

const createNote = asyncHandler(async (req, res) => {
    const { user, title, text } = req.body

    if(!user || !title || !text) {
        return res.status(400).json({ message: "All fields are required" })
    }

    // Check for duplicate title
    const duplicate = await Note.findOne({ user, title }).lean().exec()

    if(duplicate) {
        return res.status(400).json({ message: "This user already have a note with this title" })
    }

    const noteObject = {user, title, text}

    const note = await Note.create(noteObject)

    if(note) {
        res.status(201).json({ message: `New note ${title} created` })
    } else {
        res.status(400).json({ message: "Invalid note data recieved" })
    }
})

// @desc Update a note
// @route PATCH /notes
// @access Private

const updateNote = asyncHandler(async (req, res) => {
    const { id, user, title, text, completed } = req.body

    if(!user || !title || !text || typeof completed !== 'boolean') {
        return res.status(400).json({ message: "All fields are required" })
    }

    const note = await Note.findById(id).exec()

    if(!note) {
        return res.status(400).json({ message: "Note not found" })
    }

    const duplicate = await User.findOne({ user, title })

    if(duplicate) {
        return res.status(409).json({ message: "Duplicate Titles" })
    }

    note.user = user
    note.title = title
    note.text = text
    note.completed = completed

    const updatedNote = await note.save()

    res.json({ message: `${updatedNote.title} updated!` })
    
})

// @desc Delete a note
// @route DELETE /notes
// @access Private

const deleteNote = asyncHandler(async (req, res) => {
    const { id } = req.body

    if(!id) {
        return res.status(400).json({ message: "Note ID required" })
    }

    const note = await Note.findById(id)

    if(!note) {
        return res.status(400).json({ message: "Note not found" })
    }

    const result = await note.deleteOne()
    const reply = `Note ${note.title} with User ID ${note.user} deleted`
    
    res.json(reply)
})

module.exports = {
    getAllNotes,
    createNote,
    updateNote,
    deleteNote
}